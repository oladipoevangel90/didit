const express = require('express')

let UserArray = []

const path = require('path')

const app = express()

const port = 3000

const morgan = require('morgan')

const bodyParser = require('body-parser')

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use(express.static(path.join(__dirname, './public')))

app.use(morgan('dev'))

app.get('/', (req, res) => {
    res.render("didit", {pageTitle:'Home'})
})

app.set('views', path.join(__dirname, 'views'))
app.set('view engine','ejs')

app.post('/signup', (req, res) => {
    let userObject = req.body
    UserArray.push(userObject)
    console.log(UserArray)
    res.redirect('/')
})

// app.post('/login', (req, res) => {
//     let loginObject = req.body
//     LoginArray.push(loginObject)
//     console.log(LoginArray)
//     res.redirect('/')


app.get('/fitCenter', (req, res) => {
    res.render('FitofitGym', {pageTitle:'FitofitGym'})
})

app.listen(port, () => console.log(`Server is running ${port}!`))